package com.zidi.whatsmoviekotlin

import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.glide.slider.library.SliderLayout
import com.glide.slider.library.SliderTypes.DefaultSliderView
import com.glide.slider.library.Tricks.ViewPagerEx
import com.zidi.whatsmoviekotlin.adapter.RecyclerViewCollectionAdapter
import com.zidi.whatsmoviekotlin.adapter.RecyclerViewTrailerAdapter
import com.zidi.whatsmoviekotlin.api.ApiClient
import com.zidi.whatsmoviekotlin.model.*
import com.zidi.whatsmoviekotlin.model.Collection
import org.w3c.dom.Text
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class DetailActivity : AppCompatActivity(), ViewPagerEx.OnPageChangeListener {


    private var movieId = "1"
    private var movieTitle = ""
    private var collectionId = ""
    private var listProductionCompany: MutableList<ProductionCompany> = mutableListOf()
    private var listCast: MutableList<Cast> = mutableListOf()
    private var listTrailer: MutableList<VideoResult> = mutableListOf()
    private var listImages: MutableList<BackDrop> = mutableListOf()
    private var listCrew: MutableList<Crew> = mutableListOf()
    private var belongsToCollection: BelongsToCollection? = BelongsToCollection()
    private var listPart: MutableList<Part> = mutableListOf()
    private lateinit var txtJudul: TextView
    private lateinit var txtDeskripsi: TextView
    private lateinit var txtVote: TextView
    private lateinit var txtTahun: TextView
    private lateinit var txtGenre: TextView
    private lateinit var txtProduction: TextView
    private lateinit var txtCast: TextView
    private lateinit var txtCollection: TextView
    private lateinit var txtFromCollection: TextView
    private lateinit var txtTrailer: TextView
    private lateinit var txtDirector: TextView
    private lateinit var imgMovie: ImageView
    private lateinit var imgProfpic: ImageView
    private lateinit var toolbar: Toolbar
    private lateinit var mDemoSlider: SliderLayout
    private lateinit var mRecyclerView: RecyclerView
    private lateinit var apiClient: ApiClient
    private lateinit var mAdapter4: RecyclerViewTrailerAdapter
    private lateinit var colRecyclerView: RecyclerView
    private lateinit var colAdapter: RecyclerViewCollectionAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        //TODO : AdView

        toolbar = findViewById(R.id.toolbar)
        toolbar.title = ""
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        initCollapsingToolbar()

        //TODO : get Intent's data
        val jud = intent.getStringExtra("judulOrigin")
        val des = intent.getStringExtra("deskripsi")
        val thn = intent.getStringExtra("release")
        val vote = intent.getStringExtra("vote")
        val poster = intent.getStringExtra("profpic")
        val judul = intent.getStringExtra("judul")
        val genre = intent.getStringExtra("genre")
        val voteCount = intent.getStringExtra("vote_count")
        movieId = intent.getStringExtra("id")
        movieTitle = judul

        txtJudul = findViewById(R.id.tv_movie_title)
        txtDeskripsi = findViewById(R.id.movie_desc)
        imgMovie = findViewById(R.id.img_movie_poster)
        imgMovie.visibility = View.GONE
        txtTahun = findViewById(R.id.movie_date)
        txtVote = findViewById(R.id.movie_rate)
        imgProfpic = findViewById(R.id.movie_profpic)
        txtGenre = findViewById(R.id.movie_genre)

        mDemoSlider = findViewById(R.id.slider)

        txtCast = findViewById(R.id.movie_cast)
        txtCollection = findViewById(R.id.movie_collection)
        txtFromCollection = findViewById(R.id.tv_from_collection)
        txtTrailer = findViewById(R.id.tv_trailer)
        txtDirector = findViewById(R.id.movie_director)
        txtProduction = findViewById(R.id.movie_production)

        //TODO : Inisialisasi Komponen
        Glide.with(this)
                .load("https://image.tmdb.org/t/p/w185$poster")
                .apply(RequestOptions()
                        .placeholder(R.drawable.blank_movie)
                        .error(R.drawable.blank_movie))
                .into(imgProfpic)

        txtJudul.text = jud
        txtDeskripsi.text = des
        txtTahun.text = "Release date : $thn"
        txtVote.text = "$vote/10 ($voteCount votes)"
        txtGenre.text = genre

        mRecyclerView = findViewById(R.id.rv_trailer)
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(this)
        mAdapter4 = RecyclerViewTrailerAdapter(applicationContext, listTrailer){
            val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$it"))
            val webIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=$it"))
            try{
                startActivity(appIntent)
            } catch (ex: ActivityNotFoundException){
                startActivity(webIntent)
            }
        }
        mRecyclerView.adapter = mAdapter4
        mAdapter4.notifyDataSetChanged()

        colRecyclerView = findViewById(R.id.rv_collection)
        colRecyclerView.setHasFixedSize(true)
        colRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        colAdapter = RecyclerViewCollectionAdapter(applicationContext, listPart){
            val collectIntent = Intent(this,DetailActivity::class.java)
            collectIntent.putExtra("id", it.id.toString())
            collectIntent.putExtra("judulOrigin", it.originalTitle)
            collectIntent.putExtra("judul", it.title)
            collectIntent.putExtra("deskripsi", it.overview)
            collectIntent.putExtra("release", convertDate(it.releaseDate!!))
            collectIntent.putExtra("latar", it.backdropPath.toString())
            collectIntent.putExtra("profpic", it.posterPath.toString())
            collectIntent.putExtra("genre", convertGenre(it.genreIds))
            collectIntent.putExtra("vote", it.voteAverage.toString())
            collectIntent.putExtra("vote_count", it.voteCount.toString())

            startActivity(collectIntent)
        }
        colRecyclerView.adapter = colAdapter
        colAdapter.notifyDataSetChanged()

        apiClient = ApiClient()
        //Call Movie's Detail
        requestMovieDetails()
    }



    private fun initCollapsingToolbar() {
        val collapsingToolbar: CollapsingToolbarLayout = findViewById(R.id.collapsing_toolbar)
        collapsingToolbar.title = ""

        val appBar: AppBarLayout = findViewById(R.id.app_bar)
        appBar.setExpanded(true)

        appBar.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {

            var isVisible = true
            var scrollRange = -1

            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {

                if (scrollRange == -1) {
                    scrollRange = appBar.totalScrollRange
                }

                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.title = movieTitle
                    isVisible = true
                } else if (isVisible) {
                    collapsingToolbar.title = ""
                    isVisible = false
                }
            }
        })
    }

    private fun requestMovieDetails() {
        try{
            val detail: Call<Details> = apiClient.get().getDetailsMovie(
                    movieId,
                    getString(R.string.api_key),
                    "videos,images,credits")
            detail.enqueue(object : Callback<Details>{
                override fun onFailure(call: Call<Details>, t: Throwable) {
                    Toast.makeText(baseContext,t.message,Toast.LENGTH_LONG).show()
                }

                override fun onResponse(call: Call<Details>, response: Response<Details>) {
                    if (response.isSuccessful && response.body() != null){
                        val productionCompany: List<ProductionCompany> = response.body()!!.productionCompanies!!
                        val credits: List<Cast> = response.body()!!.credits!!.cast!!
                        val videos: List<VideoResult> = response.body()!!.videos!!.videoResults!!
                        val images: List<BackDrop> = response.body()!!.images!!.backdrops!!
                        val crew: List<Crew> = response.body()!!.credits!!.crew!!
                        belongsToCollection = response.body()!!.belongsToCollection
                        listProductionCompany.addAll(productionCompany)
                        listCast.addAll(credits)
                        listTrailer.addAll(videos)
                        mAdapter4.notifyDataSetChanged()
                        listImages.addAll(images)
                        listCrew.addAll(crew)

                        doApapun()
                    }
                }
            })
        } catch (e: Exception){
            Toast.makeText(baseContext,e.message,Toast.LENGTH_LONG).show()
        }
    }

    private fun requestCollection(){
        try{
            val collection: Call<Collection> = apiClient.get().getCollection(
                    collectionId,
                    getString(R.string.api_key))
            collection.enqueue(object : Callback<Collection>{
                override fun onFailure(call: Call<Collection>, t: Throwable) {
                    Toast.makeText(baseContext,t.message,Toast.LENGTH_LONG).show()
                }

                override fun onResponse(call: Call<Collection>, response: Response<Collection>) {
                    if (response.isSuccessful && response.body() != null) {
                        val parts: List<Part> = response.body()!!.parts!!
                        listPart.addAll(parts)
                        colAdapter.notifyDataSetChanged()
                    }
                }
            })

        } catch (e: Exception){
            Toast.makeText(baseContext,e.message,Toast.LENGTH_LONG).show()
        }
    }

    private fun doApapun(){

        if (listImages.size > 1){
            //TODO : using Glide Slide Library
            //TODO : using Glide Slide Library
            val requestOptions = RequestOptions()
            requestOptions.centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .error(R.drawable.blank_movie)
            for (idx in listImages.indices) {
                val sliderView = DefaultSliderView(this)
                sliderView
                        .image("https://image.tmdb.org/t/p/w780" + listImages[idx].filePath)
                        .setRequestOption(requestOptions)
                        .setBackgroundColor(Color.BLACK)
                //        .setProgressBarVisible(true);
                mDemoSlider.addSlider(sliderView)
            }
            if (listImages.size == 1) {
                mDemoSlider.stopAutoCycle()
            }
            mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Stack)
            mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom)
            mDemoSlider.setDuration(4000)
            mDemoSlider.addOnPageChangeListener(this)

        }
        else if (listImages.size == 1){
            imgMovie.visibility = View.VISIBLE
            val latar: String = "https://image.tmdb.org/t/p/w780${listImages[0].filePath}"
            Glide.with(this)
                    .load(latar)
                    .apply(RequestOptions()
                            .placeholder(R.drawable.blank_movie)
                            .error(R.drawable.blank_movie))
                    .into(imgMovie)
        }
        else {
            imgMovie.visibility = View.VISIBLE
            val latar: String = intent.getStringExtra("latar")
            Glide.with(this)
                    .load(latar)
                    .apply(RequestOptions()
                            .placeholder(R.drawable.blank_movie)
                            .error(R.drawable.blank_movie))
                    .into(imgMovie)
        }

        if (!listProductionCompany.isEmpty()) {
            //TODO : Get info detail lainnya
            //COMPANY PRODUCTION
            var prefix = ""
            var idx1 = 0
            val sb1 = StringBuilder()
            do {
                val prodCompany = listProductionCompany[idx1].name!!
                sb1.append(prefix)
                prefix = ", "
                sb1.append(prodCompany)
                idx1++
            } while (idx1 < listProductionCompany.size)
            txtProduction.text = sb1.toString()
        }

        if (!listCast.isEmpty()) {
            //CAST
            var prefix = ""
            var idx2 = 0
            val sb2 = StringBuilder()
            do {
                val cast = listCast[idx2].name!!
                sb2.append(prefix)
                prefix = ", "
                sb2.append(cast)
                idx2++
            } while (idx2 < listCast.size && idx2 <= 10)
            txtCast.text = sb2.toString()
        }

        if (!listCrew.isEmpty()) {
            //DIRECTOR
            var director = ""
            for (i in listCrew.indices) {
                if (listCrew[i].job.equals("Director")) {
                    director += listCrew[i].name
                    break
                }
            }
            txtDirector.text = director
        }

        if (belongsToCollection != null) {
            //COLLECTION
            val collection = belongsToCollection!!.name!!
            txtFromCollection.visibility = View.VISIBLE
            txtCollection.text = collection
            this.collectionId = belongsToCollection!!.id.toString()
            requestCollection()
        }

        if (listTrailer.isEmpty()) {
            txtTrailer.visibility = View.VISIBLE
        }
    }

    override fun onPageScrollStateChanged(p0: Int) {

    }

    override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {

    }

    override fun onPageSelected(p0: Int) {

    }
}
