package com.zidi.whatsmoviekotlin

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.CompoundButton
import android.widget.Switch
import android.widget.Toast
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.zidi.whatsmoviekotlin.adapter.MovieAdapter
import com.zidi.whatsmoviekotlin.adapter.RecyclerViewMovieAdapter
import com.zidi.whatsmoviekotlin.api.ApiClient
import com.zidi.whatsmoviekotlin.model.Movie
import com.zidi.whatsmoviekotlin.model.MovieResult
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var mSwipeRefreshLayout: SwipeRefreshLayout
    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mAdapter: RecyclerViewMovieAdapter
    private lateinit var navigationView: NavigationView
    private lateinit var mSwitch: Switch
    private lateinit var handler: Handler
    //    private var apiClient: ApiClient? = null
    private lateinit var apiClient: ApiClient
    private var page = 0
    private var mode = 0
    private var totalPages = 0
    private var region = ""
    private var nowDate = ""
    private var beforeDate = ""
    private var tomorrowDate = ""
    private var listMovies: MutableList<MovieResult> = mutableListOf()
    private val ADMOB_APP_ID = "ca-app-pub-1749663308384271/8963153473"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //TODO: my own Admob
//        MobileAds.initialize(this,ADMOB_APP_ID)
//        val adRequest: AdRequest = AdRequest.Builder().build()
//        adView1.loadAd(adRequest)

        //Get current Date
        val mDate: Date = Calendar.getInstance().time
        val calAgo: Calendar = Calendar.getInstance()
        calAgo.add(Calendar.MONTH, -1)
        val mDateAgo: Date = calAgo.time
        val calTom: Calendar = Calendar.getInstance()
        calTom.add(Calendar.DATE, 1)
        val mDateTom: Date = calTom.time

        val formatDate = SimpleDateFormat("yyyy-MM-dd")
        nowDate = formatDate.format(mDate)
        beforeDate = formatDate.format(mDateAgo)
        tomorrowDate = formatDate.format(mDateTom)

        setSupportActionBar(toolbar)

        mRecyclerView = findViewById(R.id.recycler_view)
        mRecyclerView.setHasFixedSize(true)

        // check screen orientation
        if (this.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            // if portrait
            mRecyclerView.layoutManager = GridLayoutManager(this, 2)
        } else {
            // if landscape
            mRecyclerView.layoutManager = GridLayoutManager(this, 4)
        }

//        mAdapter = RecyclerViewMovieAdapter(this,listMovies){
        mAdapter = RecyclerViewMovieAdapter(this, listMovies, mRecyclerView) {
            val detailIntent = Intent(this, DetailActivity::class.java)
            detailIntent.putExtra("id", it.id.toString())
            detailIntent.putExtra("judulOrigin", it.originalTitle)
            detailIntent.putExtra("judul", it.title)
            detailIntent.putExtra("deskripsi", it.overview)
            detailIntent.putExtra("release", convertDate(it.releaseDate!!))
            detailIntent.putExtra("latar", it.backdropPath)
            detailIntent.putExtra("profpic", it.posterPath)
            detailIntent.putExtra("genre", convertGenre(it.genreIds))
            detailIntent.putExtra("vote", it.voteAverage.toString())
            detailIntent.putExtra("vote_count", it.voteCount.toString())

            startActivity(detailIntent)
        }
        mRecyclerView.adapter = mAdapter
        mAdapter.notifyDataSetChanged()

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Scroll to Top", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        mSwitch = findViewById(R.id.switch1)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        if (savedInstanceState != null) {
            mode = savedInstanceState.getInt("mode")
        } else {
            mode = 0
        }
        nav_view.setNavigationItemSelectedListener(this)
        nav_view.menu.getItem(mode).isChecked = true //default

        apiClient = ApiClient()

        initViews(mode)

        mSwipeRefreshLayout = findViewById(R.id.swipe_refresh_layout)
        mSwipeRefreshLayout.setColorSchemeColors(resources.getColor(android.R.color.holo_orange_dark))

        onSwipeRefresh()


    }

    private fun onSwipeRefresh() {
        mSwipeRefreshLayout.setOnRefreshListener {
            initViews(mode)
            nav_view.menu.getItem(mode).isChecked = true
        }

    }

    private fun initViews(mode: Int) {
        handler = Handler()

        when (mode) {
            0 -> supportActionBar!!.title = "Now Playing"
            1 -> supportActionBar!!.title = "Coming Soon"
            else -> supportActionBar!!.title = "Whats Movie"
        }

        listMovies.clear()
        page = 1
        mAdapter.notifyDataSetChanged()

        //Call request Movie method
        requestMovieDatabase(page, mode, region)
        page++

        mAdapter.setOnLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                handler.postDelayed({
                    if (page <= totalPages) {
                        requestMovieDatabase(page, mode, region)
                        page++
                        mAdapter.setLoaded()
                        mAdapter.notifyDataSetChanged()
                    }
                }, 1000)
            }
        })

        mSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                region = "id"
                initViews(mode)
            } else {
                region = ""
                initViews(mode)
            }
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> {
                val aboutIntent = Intent(this, AboutActivity::class.java)
                startActivity(aboutIntent)
            }
            R.id.action_search -> {
                val searchIntent = Intent(this, SearchActivity::class.java)
                startActivity(searchIntent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_now_playing -> {
                // Handle the now playing action
                mode = 0
                initViews(mode)
            }
            R.id.nav_coming_soon -> {
                // Handle the coming soon action
                mode = 1
                initViews(mode)
            }
            R.id.nav_discover -> {
                // Handle the discover action
                val discoverIntent = Intent(this, DiscoverActivity::class.java)
                startActivity(discoverIntent)
            }
            R.id.nav_about -> {
                // Handle the about action
                val aboutIntent = Intent(this, AboutActivity::class.java)
                startActivity(aboutIntent)
            }

        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    public override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("mode", mode)
    }

    override fun onResume() {
        super.onResume()
        nav_view.menu.getItem(mode).isChecked = true //last position
    }

    //TODO 9 : create method to request Movie DataBase
    private fun requestMovieDatabase(page: Int, mode: Int, region: String) {
        try {
            when (mode) {
                0 -> {
                    Log.d("retrotif", "masuk mode 0")
//                    val mode0: Call<Movie> = apiClient?.get()!!.getNowPlaying("movie",
                    val mode0: Call<Movie> = apiClient.get().getNowPlaying("movie",
                            getString(R.string.api_key),
                            page.toString(),
                            nowDate,
                            beforeDate,
                            region)
                    mode0.enqueue(object : Callback<Movie> {
                        override fun onFailure(call: Call<Movie>, t: Throwable) {
                            Log.d("retrotif", "onFailure")
                            if (mSwipeRefreshLayout.isRefreshing) {
                                mSwipeRefreshLayout.isRefreshing = false
                            }
                            Toast.makeText(baseContext, t.message, Toast.LENGTH_LONG).show()
                            Log.d("retrotif", "gagal " + t.message)
                        }

                        override fun onResponse(call: Call<Movie>, response: Response<Movie>) {
                            Log.d("retrotif", "onResponse")
                            if (response.isSuccessful && response.body() != null) {
                                totalPages = response.body()!!.totalPages!!
                                val movies: List<MovieResult> = response.body()!!.results!!
                                listMovies.addAll(movies)
                                Log.d("retrotif", movies.toString())
                            } else {
                                Log.d("retrotif", "gagal ")
                            }

                            mAdapter.notifyDataSetChanged()
                            mAdapter.setLoaded()
                            if (mSwipeRefreshLayout.isRefreshing) {
                                mSwipeRefreshLayout.isRefreshing = false
                            }
                        }
                    })
                }

                1 -> {
                    Log.d("retrotif", "masuk mode 1")
//                    val mode1: Call<Movie> = apiClient?.get()!!.getComingSoon("movie",
                    val mode1: Call<Movie> = apiClient.get().getComingSoon("movie",
                            getString(R.string.api_key),
                            page.toString(),
                            tomorrowDate,
                            region)
                    mode1.enqueue(object : Callback<Movie> {
                        override fun onFailure(call: Call<Movie>, t: Throwable) {
                            if (mSwipeRefreshLayout.isRefreshing) {
                                mSwipeRefreshLayout.isRefreshing = false
                            }
                            Log.d("retrotif", "onFailure")
                            Toast.makeText(baseContext, t.message, Toast.LENGTH_LONG).show()
                        }

                        override fun onResponse(call: Call<Movie>, response: Response<Movie>) {
                            Log.d("retrotif", "onResponse")
                            if (response.isSuccessful && response.body() != null) {
                                totalPages = response.body()!!.totalPages!!
                                val movies: List<MovieResult> = response.body()!!.results!!
                                listMovies.addAll(movies)
                            }
                            mAdapter.notifyDataSetChanged()
                            mAdapter.setLoaded()
                            if (mSwipeRefreshLayout.isRefreshing) {
                                mSwipeRefreshLayout.isRefreshing = false
                            }
                        }
                    })
                }
            }

        } catch (e: Exception) {
            if (mSwipeRefreshLayout.isRefreshing) {
                mSwipeRefreshLayout.isRefreshing = false
            }
            Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
        }
    }
}
