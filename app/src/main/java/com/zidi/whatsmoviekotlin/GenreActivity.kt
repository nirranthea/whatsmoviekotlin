package com.zidi.whatsmoviekotlin

import android.content.Intent
import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.zidi.whatsmoviekotlin.adapter.RecyclerViewMovieAdapter
import com.zidi.whatsmoviekotlin.api.ApiClient
import com.zidi.whatsmoviekotlin.model.Movie
import com.zidi.whatsmoviekotlin.model.MovieResult
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GenreActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {


    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mAdapter2: RecyclerViewMovieAdapter
    private lateinit var handler: Handler
    private lateinit var yearsArray: Array<String>
    private lateinit var apiClient: ApiClient
    private var listMovies: MutableList<MovieResult> = mutableListOf()
    private var page = 0
    private var totalPages = 0
    private var genre = ""
    private var genreCode = ""
    private var inputYear = ""
    private val years = arrayOf("2018", "2017", "2016", "2015", "2014", "2013", "2012", "2011", "2010")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_genre)

        //TODO : adView

        yearsArray = resources.getStringArray(R.array.years_array)
        val spYear: Spinner = findViewById(R.id.spinner_year)
        var mAdapter: ArrayAdapter<String> = ArrayAdapter(this,R.layout.support_simple_spinner_dropdown_item,yearsArray)
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spYear.adapter = mAdapter
        spYear.onItemSelectedListener = this

        genreCode = intent.getStringExtra("genreCode")
        genre = intent.getStringExtra("genre")
        supportActionBar!!.title = genre

        mRecyclerView = findViewById(R.id.rv_genre)
        mRecyclerView.setHasFixedSize(true)
        // check screen orientation
        if (this.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            // if portrait
            mRecyclerView.layoutManager = GridLayoutManager(this, 2)
        } else {
            // if landscape
            mRecyclerView.layoutManager = GridLayoutManager(this, 4)
        }

        val fab: FloatingActionButton = findViewById(R.id.fab2)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Scroll to Top", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
            mRecyclerView.smoothScrollToPosition(0)
        }

        apiClient = ApiClient()
        handler = Handler()
        listMovies.clear()
        page = 1
        inputYear = years[0] //initialize
        mAdapter2 = RecyclerViewMovieAdapter(this,listMovies,mRecyclerView){
            val detailIntent = Intent(this,DetailActivity::class.java)
            detailIntent.putExtra("id",it.id.toString())
            detailIntent.putExtra("judulOrigin",it.originalTitle)
            detailIntent.putExtra("judul",it.title)
            detailIntent.putExtra("deskripsi",it.overview)
            detailIntent.putExtra("release", convertDate(it.releaseDate!!))
            detailIntent.putExtra("latar",it.backdropPath)
            detailIntent.putExtra("profpic",it.posterPath)
            detailIntent.putExtra("genre",convertGenre(it.genreIds))
            detailIntent.putExtra("vote",it.voteAverage.toString())
            detailIntent.putExtra("vote_count",it.voteCount.toString())

            startActivity(detailIntent)
        }
        mRecyclerView.adapter = mAdapter2
        mAdapter2.notifyDataSetChanged()

        mAdapter2.setOnLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                handler.postDelayed({
                    if (page <= totalPages) {
                        requestMovieGenre(page, genreCode, inputYear)
                        page++
                        mAdapter2.setLoaded()
                        mAdapter2.notifyDataSetChanged()
                    }
                }, 1000)
            }
        })

    }

    private fun requestMovieGenre(page: Int, genreCode: String, inputYear: String) {
        try{
            Log.d("retrotif","masuk mode")
            val call: Call<Movie> = apiClient.get().getGenreMovie("movie",
                    getString(R.string.api_key),
                    page.toString(),
                    genreCode,
                    inputYear)
            call.enqueue(object : Callback<Movie> {
                override fun onFailure(call: Call<Movie>, t: Throwable) {
                    Log.d("retrotif", "onFailure")
                    Toast.makeText(baseContext, t.message, Toast.LENGTH_LONG).show()
                    Log.d("retrotif", "gagal " + t.message)
                }

                override fun onResponse(call: Call<Movie>, response: Response<Movie>) {
                    Log.d("retrotif", "onResponse")
                    if (response.isSuccessful && response.body() != null) {
                        totalPages = response.body()!!.totalPages!!
                        val movies: List<MovieResult> = response.body()!!.results!!
                        listMovies.addAll(movies)
                        Log.d("retrotif", movies.toString())
                    }
                    mAdapter2.notifyDataSetChanged()
                    mAdapter2.setLoaded()
                }
            })

        } catch (e:Exception){
            Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        inputYear = yearsArray[position]
        listMovies.clear()
        page = 1
        requestMovieGenre(page,genreCode,inputYear)
        page++
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        inputYear = "2018"
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu, menu)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        menu.getItem(1).isVisible = false

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.action_settings -> {
                val aboutIntent = Intent(this, AboutActivity::class.java)
                startActivity(aboutIntent)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
