package com.zidi.whatsmoviekotlin

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import android.widget.Toast
import com.zidi.whatsmoviekotlin.adapter.RecyclerViewMovieAdapter
import com.zidi.whatsmoviekotlin.api.ApiClient
import com.zidi.whatsmoviekotlin.model.Movie
import com.zidi.whatsmoviekotlin.model.MovieResult
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchActivity : AppCompatActivity() {

    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mAdapter3: RecyclerViewMovieAdapter
    private lateinit var apiClient: ApiClient
    private lateinit var handler: Handler
    private lateinit var shandler: Handler
    private var listMovies: MutableList<MovieResult> = mutableListOf()
    private var page = 0
    private var totalPages = 0
    private var inputSearch = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        //TODO : AdView

        mRecyclerView = findViewById(R.id.rv_search)
        mRecyclerView.setHasFixedSize(true)

        // check screen orientation
        if (this.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            // if portrait
            mRecyclerView.layoutManager = GridLayoutManager(this, 2)
        } else {
            // if landscape
            mRecyclerView.layoutManager = GridLayoutManager(this, 4)
        }

        apiClient = ApiClient()
        handler = Handler()
        shandler = Handler()
        listMovies.clear()
        page = 1
        mAdapter3 = RecyclerViewMovieAdapter(this,listMovies,mRecyclerView){
            val detailIntent = Intent(this,DetailActivity::class.java)
            detailIntent.putExtra("id",it.id.toString())
            detailIntent.putExtra("judulOrigin",it.originalTitle)
            detailIntent.putExtra("judul",it.title)
            detailIntent.putExtra("deskripsi",it.overview)
            detailIntent.putExtra("release", convertDate(it.releaseDate!!))
            detailIntent.putExtra("latar",it.backdropPath)
            detailIntent.putExtra("profpic",it.posterPath)
            detailIntent.putExtra("genre",convertGenre(it.genreIds))
            detailIntent.putExtra("vote",it.voteAverage.toString())
            detailIntent.putExtra("vote_count",it.voteCount.toString())

            startActivity(detailIntent)
        }
        mRecyclerView.adapter = mAdapter3
        mAdapter3.notifyDataSetChanged()

        mAdapter3.setOnLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                handler.postDelayed({
                    if (page <= totalPages) {
                        requestMovieSearch(page, inputSearch)
                        page++
                        mAdapter3.setLoaded()
                        mAdapter3.notifyDataSetChanged()
                    }
                }, 1000)
            }
        })

    }

    private fun requestMovieSearch(page: Int, query: String) {
        try{
            val call: Call<Movie> = apiClient.get().getSearchMovie("movie",
                    getString(R.string.api_key),
                    page.toString(),
                    query)
            call.enqueue(object : Callback<Movie>{
                override fun onFailure(call: Call<Movie>, t: Throwable) {
                    Log.d("retrotif", "onFailure")
                    Toast.makeText(baseContext, t.message, Toast.LENGTH_LONG).show()
                    Log.d("retrotif", "gagal " + t.message)
                }

                override fun onResponse(call: Call<Movie>, response: Response<Movie>) {
                    Log.d("retrotif", "onResponse")
                    if (response.isSuccessful && response.body() != null) {
                        totalPages = response.body()!!.totalPages!!
                        val movies: List<MovieResult> = response.body()!!.results!!
                        listMovies.addAll(movies)
                        Log.d("retrotif", movies.toString())
                    }
                    mAdapter3.notifyDataSetChanged()
                    mAdapter3.setLoaded()
                }
            })
        } catch (e: Exception){
            Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.search_menu, menu)

        supportActionBar!!.setTitle("")
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        //TODO : searching function
        val searchManager: SearchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu.findItem(R.id.action_movie_searching).actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))

        //TODO : about keyboard
        //        searchView.setIconifiedByDefault(true);
        searchView.setIconifiedByDefault(false)
        //        searchView.setFocusable(true);
        searchView.isIconified = false
        //        searchView.requestFocusFromTouch();
        searchView.queryHint = "Search Movies..."

        val queryTextListener = object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                Toast.makeText(baseContext, "ambil search $query", Toast.LENGTH_LONG).show()
                searchView.clearFocus()

                listMovies.clear()
                page = 1
                mAdapter3.notifyDataSetChanged()
                requestMovieSearch(page, query)
                //release_mode: Log.d("Jumlah list", String.valueOf(listMovies.size()));
                //release_mode: Log.d("submit number page", String.valueOf(page));
                page++
                inputSearch = query

                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                if (newText.isNotEmpty()) {
                    searchView.setIconifiedByDefault(false)
                    //                    searchView.setFocusable(true);
                    searchView.isIconified = false

                    inputSearch = newText
                    shandler.removeCallbacksAndMessages(null)
                    shandler.postDelayed({
                        listMovies.clear()
                        page = 1
                        mAdapter3.notifyDataSetChanged()
                        requestMovieSearch(page, inputSearch)
                        //release_mode: Log.d("Jumlah list", String.valueOf(listMovies.size()));
                        //release_mode: Log.d("change number page", String.valueOf(page));
                        page++
                    }, 1000)

                } else {
                    searchView.setIconifiedByDefault(false)
                }


                return true
            }

        }
        searchView.setOnQueryTextListener(queryTextListener)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
