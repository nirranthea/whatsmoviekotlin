package com.zidi.whatsmoviekotlin

interface OnLoadMoreListener {
    fun onLoadMore()
}