package com.zidi.whatsmoviekotlin.api

import com.zidi.whatsmoviekotlin.model.Details
import com.zidi.whatsmoviekotlin.model.Movie
import com.zidi.whatsmoviekotlin.model.Collection
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface {

    @GET("https://api.themoviedb.org/3/discover/{movie_id}")
    fun getNowPlaying(@Path("movie_id") movie_id: String, @Query("api_key") api_key: String, @Query("page") page: String,
                               @Query("primary_release_date.lte") date_lte: String, @Query("primary_release_date.gte") date_gte: String,
                               @Query("region") region: String): Call<Movie>

    @GET("https://api.themoviedb.org/3/discover/{movie_id}")
    fun getComingSoon(@Path("movie_id") movie_id: String, @Query("api_key") api_key: String, @Query("page") page: String,
                               @Query("primary_release_date.gte") date_gte: String,
                               @Query("region") region: String): Call<Movie>

    @GET("https://api.themoviedb.org/3/search/{movie_id}")
    fun getSearchMovie(@Path("movie_id") movie_id: String, @Query("api_key") api_key: String, @Query("page") page: String,
                                @Query("query") query: String): Call<Movie>

    @GET("https://api.themoviedb.org/3/discover/{movie_id}")
    fun getGenreMovie(@Path("movie_id") movie_id: String, @Query("api_key") api_key: String, @Query("page") page: String,
                               @Query("with_genres") genre: String, @Query("primary_release_year") year: String): Call<Movie>

    @GET("https://api.themoviedb.org/3/movie/{movie_id}")
    fun getDetailsMovie(@Path("movie_id") movie_id: String, @Query("api_key") api_key: String,
                                 @Query("append_to_response") append: String): Call<Details>

    @GET("https://api.themoviedb.org/3/collection/{movie_id}")
    fun getCollection(@Path("movie_id") movie_id: String, @Query("api_key") api_key: String): Call<Collection>
}