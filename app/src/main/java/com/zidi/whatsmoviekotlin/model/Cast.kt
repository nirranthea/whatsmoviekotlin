package com.zidi.whatsmoviekotlin.model

import com.google.gson.annotations.SerializedName

data class Cast(

        @SerializedName("cast_id")
        var castId: Int? = null,

        @SerializedName("character")
        var character: String? = null,

        @SerializedName("credit_id")
        var creditId: String? = null,

        @SerializedName("gender")
        var gender: Int? = null,

        @SerializedName("id")
        var id: Int? = null,

        @SerializedName("name")
        var name: String? = null,

        @SerializedName("order")
        var order: Int? = null,

        @SerializedName("profile_path")
        var profilePath: String? = null
)