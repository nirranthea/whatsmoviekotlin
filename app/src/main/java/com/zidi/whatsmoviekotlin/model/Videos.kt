package com.zidi.whatsmoviekotlin.model

import com.google.gson.annotations.SerializedName

data class Videos(
        @SerializedName("results")
        var videoResults: List<VideoResult>? = null

)