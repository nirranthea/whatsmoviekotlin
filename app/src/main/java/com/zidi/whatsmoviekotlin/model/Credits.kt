package com.zidi.whatsmoviekotlin.model

import com.google.gson.annotations.SerializedName

data class Credits(

        @SerializedName("cast")
        var cast: List<Cast>? = null,

        @SerializedName("crew")
        var crew: List<Crew>? = null
)