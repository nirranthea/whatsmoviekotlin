package com.zidi.whatsmoviekotlin.model

import com.google.gson.annotations.SerializedName

data class Part(

        @SerializedName("adult")
        var adult: Boolean? = null,

        @SerializedName("backdrop_path")
        var backdropPath: Any? = null,

        @SerializedName("genre_ids")
        var genreIds: List<Int>? = null,

        @SerializedName("id")
        var id: Int? = null,

        @SerializedName("original_language")
        var originalLanguage: String? = null,

        @SerializedName("original_title")
        var originalTitle: String? = null,

        @SerializedName("overview")
        var overview: String? = null,

        @SerializedName("poster_path")
        var posterPath: Any? = null,

        @SerializedName("release_date")
        var releaseDate: String? = null,

        @SerializedName("title")
        var title: String? = null,

        @SerializedName("video")
        var video: Boolean? = null,

        @SerializedName("vote_average")
        var voteAverage: Double? = null,

        @SerializedName("vote_count")
        var voteCount: Int? = null
)