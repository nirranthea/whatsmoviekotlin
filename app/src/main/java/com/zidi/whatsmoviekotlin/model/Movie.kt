package com.zidi.whatsmoviekotlin.model

import com.google.gson.annotations.SerializedName

//TODO 4 : create POJO from JSON response ("http://www.jsonschema2pojo.org/")
data class Movie(

        @SerializedName("page")
        var page: Int? = null,

        @SerializedName("total_results")
        var totalResults: Int? = null,

        @SerializedName("total_pages")
        var totalPages: Int? = null,

        @SerializedName("results")
        var results: List<MovieResult>? = null
)
