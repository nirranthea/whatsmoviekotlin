package com.zidi.whatsmoviekotlin.model

import com.google.gson.annotations.SerializedName

data class Images(

        @SerializedName("backdrops")
        var backdrops: List<BackDrop>? = null,

        @SerializedName("posters")
        var posters: List<Poster>? = null
)