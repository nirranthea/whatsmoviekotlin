package com.zidi.whatsmoviekotlin

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun convertDate(date: String): String {
    val dateFormat = SimpleDateFormat("yyyy-MM-dd")
    var myDate: Date? = null
    try {
        myDate = dateFormat.parse(date)
    } catch (e: ParseException) {
        e.printStackTrace()
    }

    val timeFormat = SimpleDateFormat("dd MMM yyy")
    return timeFormat.format(myDate)
}

fun convertGenre(listGenre: List<Int>?): String{
    val mapGenre = GenresData.getListGenres()
    var prefix = ""
    val sb = StringBuilder()
    if (listGenre != null) {
        for (i in 0 until listGenre.size) {
            val genre_id = listGenre[i]
            val nameGenre = mapGenre[genre_id]!!
            sb.append(prefix)
            prefix = ", "
            sb.append(nameGenre)
        }
    }
    return sb.toString()
}