package com.zidi.whatsmoviekotlin

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler

class SplashActivity : AppCompatActivity() {

    //Duration of wait
    private val SPLASH_DISPLAY_LENGTH = 1500

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        //New Handler to start the Main-Activity and close this Splash-Screen after some seconds
        Handler().postDelayed({
            //Create an Intent that will start the Main-Activity
            //                Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
            val mainIntent = Intent(this, MainActivity::class.java)
            this.startActivity(mainIntent)
            this.finish()
        }, SPLASH_DISPLAY_LENGTH.toLong())
    }
}
