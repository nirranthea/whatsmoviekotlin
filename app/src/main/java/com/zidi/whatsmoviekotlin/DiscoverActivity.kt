package com.zidi.whatsmoviekotlin

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import com.zidi.whatsmoviekotlin.adapter.RecyclerViewGenreAdapter

class DiscoverActivity : AppCompatActivity() {

    private lateinit var gRecyclerView: RecyclerView
    private lateinit var gAdapter: RecyclerViewGenreAdapter
    private lateinit var genreList: ArrayList<String>
    private lateinit var genreCodeList: ArrayList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_discover)

        supportActionBar?.title = "Discover"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        gRecyclerView = findViewById(R.id.rv_discover)
        gRecyclerView.setHasFixedSize(true)
        gRecyclerView.layoutManager = LinearLayoutManager(this)

        genreList = ArrayList<String>()
        genreList.addAll(GenresData.getArrayListGenres())
        genreCodeList = ArrayList<String>()
        genreCodeList.addAll(GenresData.getArrayListCodeGenres())
        gAdapter = RecyclerViewGenreAdapter(this,genreList,genreCodeList){
            val genreIntent = Intent(this,GenreActivity::class.java)
            genreIntent.putExtra("genre",it[0])
            genreIntent.putExtra("genreCode",it[1])
            startActivity(genreIntent)
        }
        gRecyclerView.adapter = gAdapter
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home){
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
