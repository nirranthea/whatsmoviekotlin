package com.zidi.whatsmoviekotlin

import java.util.ArrayList
import java.util.HashMap


class GenresData {

    companion object {
        val genres = arrayOf(
        arrayOf("28", "Action"),
        arrayOf("12", "Adventure"),
        arrayOf("16", "Animation"),
        arrayOf("35", "Comedy"),
        arrayOf("80", "Crime"),
        arrayOf("99", "Documentary"),
        arrayOf("18", "Drama"),
        arrayOf("10751", "Family"),
        arrayOf("14", "Fantasy"),
        arrayOf("36", "History"),
        arrayOf("27", "Horror"),
        arrayOf("10402", "Music"),
        arrayOf("9648", "Mystery"),
        arrayOf("10749", "Romance"),
        arrayOf("878", "Science Fiction"),
        arrayOf("10770", "TV Movie"),
        arrayOf("53", "Thriller"),
        arrayOf("10752", "War"),
        arrayOf("37", "Western"))

        fun getListGenres(): java.util.HashMap<Int, String> {
            val mapGenre = HashMap<Int, String>()
            for (i in genres.indices) {
                mapGenre[Integer.parseInt(genres[i][0])] = genres[i][1]
            }
            return mapGenre
        }

        fun getArrayListGenres(): ArrayList<String> {
            val arrayGenre = ArrayList<String>()
            for (i in genres.indices) {
                arrayGenre.add(i, genres[i][1])
            }
            return arrayGenre
        }

        fun getArrayListCodeGenres(): ArrayList<String> {
            val arrayCodeGenre = ArrayList<String>()
            for (i in genres.indices) {
                arrayCodeGenre.add(i, genres[i][0])
            }
            return arrayCodeGenre
        }
    }
}