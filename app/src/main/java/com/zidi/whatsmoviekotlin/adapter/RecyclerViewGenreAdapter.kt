package com.zidi.whatsmoviekotlin.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.zidi.whatsmoviekotlin.R

class RecyclerViewGenreAdapter(private val context: Context,
                               private val genreList: List<String>,
                               private val genreCodeList: List<String>,
                               private val listener: (List<String>) -> Unit)
    :RecyclerView.Adapter<RecyclerViewGenreHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): RecyclerViewGenreHolder {
        val layoutView = LayoutInflater.from(parent.context).inflate(R.layout.item_discover, null)
        return RecyclerViewGenreHolder(layoutView)
    }

    override fun getItemCount(): Int {
        return this.genreList.size
    }

    override fun onBindViewHolder(holder: RecyclerViewGenreHolder, position: Int) {
        var genreIcon = 0;
        when(position){
            0 -> genreIcon = R.mipmap.action
            1 -> genreIcon = R.mipmap.adventure
            2 -> genreIcon = R.mipmap.animation
            3 -> genreIcon = R.mipmap.comedy
            4 -> genreIcon = R.mipmap.crime
            5 -> genreIcon = R.mipmap.documentary
            6 -> genreIcon = R.mipmap.drama
            7 -> genreIcon = R.mipmap.family
            8 -> genreIcon = R.mipmap.fantasy
            9 -> genreIcon = R.mipmap.historical
           10 -> genreIcon = R.mipmap.horror
           11 -> genreIcon = R.mipmap.music
           12 -> genreIcon = R.mipmap.mystery
           13 -> genreIcon = R.mipmap.romance
           14 -> genreIcon = R.mipmap.sciencefiction
           15 -> genreIcon = R.drawable.ic_tv
           16 -> genreIcon = R.mipmap.thriller
           17 -> genreIcon = R.mipmap.war
           18 -> genreIcon = R.mipmap.western
        }
        holder.bindItem(genreList[position],genreCodeList[position],genreIcon, listener)
    }

}

class RecyclerViewGenreHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val mGenre: TextView = view.findViewById(R.id.tv_genre)
    private val mIcon: ImageView = view.findViewById(R.id.iv_genre)
    private var mCodeGenre: String = ""

    fun bindItem(genre: String, codeGenre: String, genreIcon: Int, listener: (List<String>) -> Unit){
        mGenre.text = genre
        mCodeGenre = codeGenre
        mIcon.setImageResource(genreIcon)

        itemView.setOnClickListener{
            val list = ArrayList<String>()
            list.add(genre)
            list.add(codeGenre)
            listener(list)
        }
    }
}
