package com.zidi.whatsmoviekotlin.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.zidi.whatsmoviekotlin.R
import com.zidi.whatsmoviekotlin.model.VideoResult

class RecyclerViewTrailerAdapter(private val context: Context,
                                 private val trailerList: List<VideoResult>,
                                 private val listener: (String) -> Unit)
    :RecyclerView.Adapter<RecyclerViewTrailerHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): RecyclerViewTrailerHolder {
        val layoutView = LayoutInflater.from(parent.context).inflate(R.layout.item_video,null)
        return RecyclerViewTrailerHolder(layoutView)
    }

    override fun getItemCount(): Int {
        return this.trailerList.size
    }

    override fun onBindViewHolder(holder: RecyclerViewTrailerHolder, position: Int) {
        holder.bindItem(trailerList[position],context,listener)
    }

}

class RecyclerViewTrailerHolder(view: View): RecyclerView.ViewHolder(view) {

    private val tvTrailer: TextView = view.findViewById(R.id.itm_trailer)
    private var keyTrailer: String = ""

    fun bindItem(videos: VideoResult, context: Context, listener: (String) -> Unit){
        tvTrailer.text = videos.name
        keyTrailer = videos.key.toString()

        itemView.setOnClickListener {
            listener(keyTrailer)
        }

    }
}
