package com.zidi.whatsmoviekotlin.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zidi.whatsmoviekotlin.OnLoadMoreListener
import com.zidi.whatsmoviekotlin.R
import com.zidi.whatsmoviekotlin.model.MovieResult

class MovieAdapter(private val context: Context,
                   private val listMovie: List<MovieResult>,
                   private val listener: (MovieResult)-> Unit)
    :RecyclerView.Adapter<MovieHolder>(){

    private var loading: Boolean = false
    private var onLoadMoreListener: OnLoadMoreListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieHolder {
        val layoutView = LayoutInflater.from(parent.context).inflate(R.layout.item_view, null)
        return MovieHolder(layoutView)
    }

    override fun getItemCount(): Int = listMovie.size


    override fun onBindViewHolder(holder: MovieHolder, position: Int) {
        holder.bindItem(listMovie[position],listener)
    }

    fun setLoaded() {
        loading = false
    }


    fun setOnLoadMoreListener(onLoadMoreListener: OnLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener
    }

}

class MovieHolder(view: View): RecyclerView.ViewHolder(view) {

    fun bindItem(movies: MovieResult, listener: (MovieResult) -> Unit){

    }
}
