package com.zidi.whatsmoviekotlin.adapter

import android.content.Context
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.zidi.whatsmoviekotlin.GenresData
import com.zidi.whatsmoviekotlin.OnLoadMoreListener
import com.zidi.whatsmoviekotlin.R
import com.zidi.whatsmoviekotlin.model.MovieResult

class RecyclerViewMovieAdapter(private val context: Context,
                               private val movieList: List<MovieResult>,
                               private val recyclerView: RecyclerView,
                               private val listener: (MovieResult) -> Unit)
    : RecyclerView.Adapter<RecyclerViewMovieHolder>() {

    //for load more
    private val VIEW_ITEM = 1
    private val VIEW_PROG = 0
    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private val visibleThreshold = 2
    private var lastVisibleItem: Int = 0
    private var totalItemCount:Int = 0
    private var loading: Boolean = false
    private var onLoadMoreListener: OnLoadMoreListener? = null

    //from constructor Java
    init{
        val gridLayoutManager = recyclerView.layoutManager as GridLayoutManager
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                totalItemCount = gridLayoutManager.itemCount
                lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition()
                if (!loading && totalItemCount <= lastVisibleItem + visibleThreshold) {
                    //End has been reached, do something
                    onLoadMoreListener?.onLoadMore()
                    loading = true
                }
            }
        })
    }

    override fun getItemViewType(position: Int): Int {
        return if (movieList[position] != null) VIEW_ITEM else VIEW_PROG
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewMovieHolder {
        val layoutView = LayoutInflater.from(parent.context).inflate(R.layout.item_view, null)
        return RecyclerViewMovieHolder(layoutView)
    }

    override fun onBindViewHolder(holder: RecyclerViewMovieHolder, position: Int) {
        holder.bindItem(movieList[position],context,listener)
    }

    override fun getItemCount(): Int {
        return this.movieList.size
    }

    fun setLoaded() {
        loading = false
    }


    fun setOnLoadMoreListener(onLoadMoreListener: OnLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener
    }
}

class RecyclerViewMovieHolder(view: View) : RecyclerView.ViewHolder(view){

    private val title: TextView = view.findViewById(R.id.movie_title)
    private val rating: TextView = view.findViewById(R.id.movie_star)
    private val image: ImageView = view.findViewById(R.id.movie_thumbnail)

    fun bindItem(movieResult: MovieResult, context: Context, listener: (MovieResult) -> Unit){
        title.text = movieResult.title
        rating.text = movieResult.voteAverage.toString()
        Glide.with(context)
                .load("https://image.tmdb.org/t/p/w185" + movieResult.posterPath)
                .apply(RequestOptions()
                        .placeholder(R.drawable.movie_blank_portrait)
                        .error(R.drawable.movie_blank_portrait))
                .into(image)

        itemView.setOnClickListener{
            listener(movieResult)
        }
    }
}