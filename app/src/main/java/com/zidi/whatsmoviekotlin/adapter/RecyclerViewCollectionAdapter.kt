package com.zidi.whatsmoviekotlin.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.zidi.whatsmoviekotlin.R
import com.zidi.whatsmoviekotlin.model.Part

class RecyclerViewCollectionAdapter(private val context: Context,
                                    private val partList: List<Part>,
                                    private val listener: (Part) -> Unit)
    :RecyclerView.Adapter<RecyclerViewCollectionHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewCollectionHolder {
        val layoutView = LayoutInflater.from(parent.context).inflate(R.layout.item_collection, null)
        return RecyclerViewCollectionHolder(layoutView)
    }

    override fun getItemCount(): Int {
        return this.partList.size
    }

    override fun onBindViewHolder(holder: RecyclerViewCollectionHolder, position: Int) {
        holder.bindItem(partList[position],context,listener)
    }


}

class RecyclerViewCollectionHolder(view: View) : RecyclerView.ViewHolder(view){

    private val txtCollection: TextView = view.findViewById(R.id.tv_collection)
    private val imgCollection: ImageView = view.findViewById(R.id.img_collection)

    fun bindItem(part: Part, context: Context, listener: (Part) -> Unit){
        txtCollection.text = part.title
        Glide.with(context)
                .load("https://image.tmdb.org/t/p/w185" + part.posterPath)
                .apply(RequestOptions()
                        .placeholder(R.drawable.movie_blank_portrait)
                        .error(R.drawable.movie_blank_portrait))
                .into(imgCollection)

        itemView.setOnClickListener {
            listener(part)
        }
    }

}
